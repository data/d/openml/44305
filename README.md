# OpenML dataset: Meta_Album_AWA_Mini

https://www.openml.org/d/44305

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Animals with Attributes Dataset (Mini)**
***
The original Animals with Attributes 2 (AWA) dataset (https://cvml.ist.ac.at/AwA2/) was designed to benchmark transfer-learning algorithms, in particular attribute base classification and zero-shot learning. It has more than 37 000 images from 50 animals, where each animal corresponds to a class. The images of this dataset were collected from public sources, such as Flickr, in 2016, considering only images licensed for free use and redistribution. Each class can have 100 to 1 645 images with a resolution from 100x100 to 1 893x1 920 px. To preprocess this dataset, we cropped the images from either side to make them square. In case an image has a resolution lower than 128 px, the squared images are done by either duplicating the top and bottom-most 3 rows or the left and right most 3 columns based on the orientation of the original image. Lastly, the square images are resized into 128x128 px using an anti-aliasing filter.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/AWA.png)

**Meta Album ID**: LR_AM.AWA  
**Meta Album URL**: [https://meta-album.github.io/datasets/AWA.html](https://meta-album.github.io/datasets/AWA.html)  
**Domain ID**: LR_AM  
**Domain Name**: Large Aninamls  
**Dataset ID**: AWA  
**Dataset Name**: Animals with Attributes  
**Short Description**: Mamals dataset for image classification  
**\# Classes**: 50  
**\# Images**: 2000  
**Keywords**: mammals, animals,  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: Creative Commons  
**License URL(original data release)**: https://cvml.ist.ac.at/AwA2/
 
**License (Meta-Album data release)**: Creative Commons  
**License URL (Meta-Album data release)**: [https://cvml.ist.ac.at/AwA2/](https://cvml.ist.ac.at/AwA2/)  

**Source**: Animals with attributes 2  
**Source URL**: https://cvml.ist.ac.at/AwA2/  
  
**Original Author**: Christoph H. Lampert, Bernt Schiele, Zeynep Akata  
**Original contact**: chl@ist.ac.at  

**Meta Album author**: Dustin Carrion  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@ARTICLE{8413121,
  author={Xian, Yongqin and Lampert, Christoph H. and Schiele, Bernt and Akata, Zeynep},
  journal={IEEE Transactions on Pattern Analysis and Machine Intelligence}, 
  title={Zero-Shot Learning - A Comprehensive Evaluation of the Good, the Bad and the Ugly}, 
  year={2019},
  volume={41},
  number={9},
  pages={2251-2265},
  doi={10.1109/TPAMI.2018.2857768}
}

```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44275)  [[Extended]](https://www.openml.org/d/44338)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44305) of an [OpenML dataset](https://www.openml.org/d/44305). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44305/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44305/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44305/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

